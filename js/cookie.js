function fadeOut(opacity){
    if(opacity <= 0){
        document.getElementById('cookieConsent').style.display = 'none';
        setCookie('cookieAuthorized', 'true', 365);
        return;
    }

    setTimeout(function(){
        document.getElementById('cookieConsent').style.opacity = opacity;
        opacity = opacity - 0.01;
        fadeOut(opacity);
    },10);
}

function setCookie(name, value, days) {
    const date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    const expires = "expires=" + date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}