window.addEventListener("load", onVrViewLoad);

function onVrViewLoad() {
    // Selector "#vrview" finds element with id "vrview".
    var vrView = new VRView.Player("#vrview", {
        image: "https://s.studiobinder.com/wp-content/uploads/2019/11/360-Video-Featured-StudioBinder-Compressed.jpg",
        width: "560px",
        height: "315px"
    });
}

function load(){
    console.log("Site web développé par Timothé Medico: https://www.baragouin.fr")
}

function openDetails(param){
    document.getElementById("map").style.display = "none";
    document.getElementById("details").style.display = "block";

    switch(param){
        case "sciences":
            document.getElementById("title").innerText = "Sciences";
            document.getElementById("sciencesDetails").style.display = "block";
            break;

        case "languages":
            document.getElementById("title").innerText = "Langues";
            document.getElementById("languagesDetails").style.display = "block";
            break;

        case "history":
            document.getElementById("title").innerText = "Histoire";
            document.getElementById("historyDetails").style.display = "block";
            break;
    }
}

function closeDetails(){
    document.getElementById("map").style.display = "block";
    document.getElementById("details").style.display = "none";
    const elements = document.getElementsByClassName("sectionDetails");
    for(let i = 0; i < elements.length; i++) {
        elements[i].style.display = "none";
    }
}