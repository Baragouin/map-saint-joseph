<?php
//HEADER PART
define("HEADER_HOME", "Home");
define("HEADER_HISTORY", "A bit of history");
define("HEADER_VISIT", "\"Virtual Tour\" project");
define("HEADER_MAKING_OFF", "Making Off");
define("HEADER_INTERVIEWS", "Interviews");
define("HEADER_LANG", "Choice of language");

// FOOTER PART
define("FOOTER_EXPLANATION", "This web site was created during an SNT project by Saint-Joseph high school student's");
define("FOOTER_YEAR", "Year 2019 - 2020");
define("FOOTER_NAMES", "Names of participants");

//COOKIE PART
define("COOKIE_EXPLANATION", "By browsing this site, I accept the use of cookies for personalization purposes.");
define("COOKIE_MORE_INFO", "More informations");
define("COOKIE_AGREE", "I agree");

//choose-lang.php
define("CHOOSE_LANG", "Choose your language");
define("CHOOSE_LANG_CONFIRM", "Confirm");