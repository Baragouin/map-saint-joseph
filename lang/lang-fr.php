<?php
//HEADER PART
define("HEADER_HOME", "Acceuil");
define("HEADER_HISTORY", "Un peu d'histoire");
define("HEADER_VISIT", "Projet \"Visite virtuelle\"");
define("HEADER_MAKING_OFF", "Making off");
define("HEADER_INTERVIEWS", "Interviews");
define("HEADER_LANG", "Choix de la langue");

// FOOTER PART
define("FOOTER_EXPLANATION", "Ce site à été créée au cours d'un projet de SNT par les élèves de Seconde du lycée Saint Joseph à Thonon-les-Bains");
define("FOOTER_YEAR", "Année 2019 - 2020");
define("FOOTER_NAMES", "Nom des participants");

//COOKIE PART
define("COOKIE_EXPLANATION", "En navigant sur ce site, j'accepte l'utilisation des cookies à des fins de personnalisations.");
define("COOKIE_MORE_INFO", "Plus d'informations");
define("COOKIE_AGREE", "J'accepte");

//choose-lang.php
define("CHOOSE_LANG", "Choisissez votre langue");
define("CHOOSE_LANG_CONFIRM", "Confirmer");

//TOUR PART
