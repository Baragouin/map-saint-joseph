<?php
    if(isset($_COOKIE['lang'])){
        $lang = $_COOKIE['lang'];
    } else {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        setcookie("lang", $lang, time() + 365*24*3600, '/');
    }

    switch ($lang){
        case 'fr':
            include('lang-fr.php');
            break;

        case 'en':
            include('lang-en.php');
            break;

        default:
            include('lang-fr.php');
            break;
    }