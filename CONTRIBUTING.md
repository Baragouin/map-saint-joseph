# Contribution

Ce fichier ressences des règles basiques pour la bonne collaboration et continuité du projet.

Avant de se lancer dans un changement, merci d'en parler avec moi (Baragouin#0001), je vous donnerai d'éventuelles consignes.
Sauf exception, le travail se fait en utilisant une nouvelle branche copié sur la branche **master**, ayant un nom permettant de comprendre l'ajout ou la modification effectué. Une fois la tâche finalisée, il vous suffira de commit, de push et ensuite de créer une merge request vers la branche master. 
Une fois le code validé par l'ensemble de l'équipe et les conflits réglés via un **merge-request**, cette branche sera merge vers la branche master par moi-même.

## Conduite vis-à-vis du code

### Code général

La rédaction du code se fait en suivant les derniers standards en vigueur. Merci de respecter les conventions afin de permettre une meilleure lisibilitée du code, de plus veillez à donner un nom clair à vos objets et méthodes, de ne pas faire de lignes trop longues et à ne pas faire de fonctions trop grosses.

### Commentaires

Votre code doit être commenté et documenté dans l'optique d'un usage futur par des personnes étrangères à la réalisation.
Ainsi, vous êtes priés de suivre les présentes règles :
* les commentaires doivent être écrits en français (tout comme les commits)
* les phrases descriptives doivent être courtes et claires. Ainsi éviter, les "permets de" où toute autre forme pouvant être raccourcie

### Commits

Les commits doivent porter un nom permettant de comprendre des changements opérés dans ces derniers. En outre, il est demandé de ne pas toucher aux fichiers gérant l'intégration continue (.gitlab-ci.yml). 
Avant d'envoyer un commit, assurez-vous également de ne pas envoyer de fichiers "poubelles" (.idea notamment).