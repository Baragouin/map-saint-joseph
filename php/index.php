<!DOCTYPE html>
<html>
<head>
    <title>Visite virtuelle de Saint-Joseph</title>
    <meta charset="utf-8" lang="fr">

    <?php include (dirname(__DIR__) . '/style/required.php') ?>

    <link rel="stylesheet" type="text/css" href="../style/index.css">
</head>
<body onload="load()">
    <?php
    require('../lang/lang.php');
    include("header.php");
    ?>

    <div id="main_wrapper">

    </div>

    <?php
        include("footer.php");
        include('cookie.php');
    ?>
</body>
</html>