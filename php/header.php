<header>
    <a id="logo_link" href="https://www.stjothonon.org/" target="_blank">
        <img id="logo" src="https://www.stjothonon.org/wp-content/uploads/2015/10/pictoStJo.png" alt="Logo de Saint-Joseph">
    </a>

    <nav>
        <ul>
            <li>
                <a href="index.php">
                    <img src="../images/home.png">
                    <p><?php echo HEADER_HOME; ?></p>
                </a>
            </li>
            <li>
                <a href="">
                    <img src="../images/history.png">
                    <p><?php echo HEADER_HISTORY; ?></p>
                </a>
            </li>
            <li>
                <a href="tour.php">
                    <img src="../images/tour.png">
                    <p><?php echo HEADER_VISIT; ?></p>
                </a>
            </li>
            <li>
                <a href="">
                    <img src="../images/makingoff.png">
                    <p><?php echo HEADER_MAKING_OFF; ?></p>
                </a>
            </li>
            <li>
                <a href="">
                    <img src="../images/interviews.png">
                    <p><?php echo HEADER_INTERVIEWS; ?></p>
                </a>
            </li>
            <li>
                <a href="choose-lang.php">
                    <img src="../images/languages.png">
                    <p><?php echo HEADER_LANG; ?></p>
                </a>
            </li>
        </ul>
    </nav>
</header>