<footer>
    <a href="#credentials">
        <img id="blue-triangle" src="../images/triangle-blue.svg">
    </a>

    <section id="explanation">
        <p><?php echo FOOTER_EXPLANATION; ?></p>
        <h3><?php echo FOOTER_YEAR; ?></h3>
    </section>
    <section id="credentials">
        <h2><?php echo FOOTER_NAMES; ?></h2>

        <div id="names">
            <ul>
                <li>Timothé MEDICO</li>
                <li>Antoine MOUCHET</li>
                <li>Paul ROCHE</li>
            </ul>

            <ul>
                <li></li>
            </ul>
        </div>
    </section>

    <img id="grey-triangle" src="../images/triangle-grey.svg">
</footer>