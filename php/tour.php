<!DOCTYPE html>
<html>
<head>
    <title>Visite virtuelle de Saint-Joseph</title>
    <meta charset="utf-8" lang="fr">

    <?php include (dirname(__DIR__) . '/style/required.php') ?>

    <link rel="stylesheet" type="text/css" href="../style/tour.css">
    <link rel="stylesheet" type="text/css" href="../style/map.css">
    <link rel="stylesheet" type="text/css" href="../style/details.css">

    <script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script>
    <script type="text/javascript" src="../js/animation.js"></script>
</head>
<body>
    <?php
    require('../lang/lang.php');
    include("header.php");
    ?>

    <div id="main_wrapper">
        <h1>Visite virtuelle de l'établissement</h1>

        <div id="container">
            <div id="map">
                <div class="row">
                    <div id="sciences" class="section" onclick="openDetails('sciences')">
                        <p>Sciences</p>
                    </div>
                </div>

                <div class="row">
                    <div id="languages" class="section" onclick="openDetails('languages')">
                        <p>Langues</p>
                    </div>
                    <div id="history" class="section" onclick="openDetails('history')">
                        <p>Histoire</p>
                    </div>
                </div>
            </div>

            <div id="details">
                <img id="close" src="../images/cross.svg" onclick="closeDetails()">
                <p id="title"></p>


                <div id="content">
                    <section id="interview">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/SEIoV0cIhV4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </section>

                    <section id="view">
                        <div class="sectionDetails" id="sciencesDetails">
                            <div class="vr" id='vrSciences'>
                                <iframe width="560" height="315" src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyB6gwil6LZJcLJF2cw47o_RYzguSRFBcqk&location=46.414382,10.013988&heading=210&pitch=10&fov=35"></iframe>
                            </div>
                        </div>

                        <div class="sectionDetails" id="languagesDetails">
                            <div class="vr" id='vrLanguages'>
                                <iframe width="560" height="315" src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyB6gwil6LZJcLJF2cw47o_RYzguSRFBcqk&location=46.414382,10.013988&heading=210&pitch=10&fov=35"></iframe>
                            </div>
                        </div>

                        <div class="sectionDetails" id="historyDetails">
                            <div class="vr" id='vrHistory'>
                                <iframe width="560" height="315" src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyB6gwil6LZJcLJF2cw47o_RYzguSRFBcqk&location=46.414382,10.013988&heading=210&pitch=10&fov=35"></iframe>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php
    include("footer.php");
    include('cookie.php');
    ?>
</body>
</html>

<?php
