<?php
require(dirname(__DIR__) . "/lang/lang.php");

if(isset($_GET['lang'])){
    $lang = $_GET['lang'];

    setcookie('lang', '', time() - 3600, '/');
    setcookie('lang', $lang, time() + 365*24*3600, '/');
    header('Location: ../php/index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Choix de la langue</title>
    <?php include(dirname(__DIR__) . '/style/required.php') ?>
    <link rel="stylesheet" type="text/css" href="../style/choose-lang.css">
</head>
<body>
    <?php include(dirname(__DIR__) . '/php/header.php'); ?>

    <div id="main_wrapper">
        <div id="selection">
            <h1><?php echo CHOOSE_LANG; ?></h1>

            <form method="get" action="choose-lang.php">
                <div id="languages">
                    <div class="line">
                        <input type="radio" id="french" name="lang" value="fr" checked="checked">
                        <label for="french">Français</label>

                        <input type="radio" id="english" name="lang" value="en">
                        <label for="english">English</label>

                        <input type="radio" id="dutch" name="lang" value="de">
                        <label for="dutch">Deutsch</label>
                    </div>

                    <div class="line">
                        <input type="radio" id="spanish" name="lang" value="es">
                        <label for="spanish">Español</label>

                        <input type="radio" id="chinese" name="lang" value="ch">
                        <label for="chinese">中国人</label>

                        <input type="radio" id="russian" name="lang" value="ru">
                        <label for="russian">Pусский</label>
                    </div>
                </div>

                <input value="<?php echo CHOOSE_LANG_CONFIRM; ?>" type="submit">
            </form>
        </div>
    </div>

    <?php include(dirname(__DIR__) . '/php/footer.php'); ?>
</body>
</html>
