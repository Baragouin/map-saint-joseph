# Saint Joseph - Map 

[![pipeline status](https://gitlab.com/Baragouin/map-saint-joseph/badges/master/pipeline.svg)](https://gitlab.com/Baragouin/map-saint-joseph/-/commits/master)

Afin de pouvoir collaborer dans les meilleurs conditions possibles, merci de prendre connaissance des quelques règles et informations ci-dessous.
Avant de pouvoir contribuer à ce projet, vous devez savoir utiliser Git et Gitlab afin de ne pas détruire le travail des autres, voici un lien si vous avez besoin d'[AIDE](https://openclassrooms.com/fr/courses/2342361-gerez-votre-code-avec-git-et-github).

Les règles relatives au code sont également spécifiées dans le fichier [CONTRIBUTING](CONTRIBUTING.md).  
En cas de doutes, n'hésitez pas à me demander via Discord (Baragouin#0001).
